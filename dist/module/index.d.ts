import * as React from 'react';
import { Reducer } from 'redux';
declare type Mnemonic = "always" | "argument_check" | "never";
export interface IStoreExtension {
    states: string[];
    mnemonic: Mnemonic;
    fn: (states: {
        [key: string]: any;
    }, ...args: any[]) => any;
    resetCB?: () => any;
}
export interface IStoreExtensions {
    [key: string]: IStoreExtension;
}
export interface IReducerAndActions {
    reducer: Reducer;
    actions: {
        [key: string]: (...args: any[]) => any;
    };
}
export interface IReducersAndActions {
    [key: string]: IReducerAndActions;
}
export declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export interface WithStoreProps {
    store: any;
    store_actions: any;
    store_ext: any;
}
export declare function withStore<P extends WithStoreProps>(WrappedComponent: React.ComponentType<P>, states?: string[]): React.ComponentClass<Omit<P, keyof WithStoreProps>>;
export declare namespace withStore {
    const addState: (state_key: string, reducer_and_actions: IReducerAndActions) => {
        actions: {
            [key: string]: (...args: any[]) => any;
        };
        getState: () => {};
        addUpdateCallback: (cb: () => void) => void;
    };
    const addStates: (reducers_and_actions: IReducersAndActions) => void;
    const addExtensions: (extensions: IStoreExtensions) => void;
    const actions: {
        [key: string]: {
            [key: string]: (...args: any[]) => any;
        };
    };
    const extensions: {
        [key: string]: (...args: any[]) => any;
    };
    const getState: <StoreDefinition extends {}>() => StoreDefinition;
    const subscribe: (listener: () => void) => import("redux").Unsubscribe;
}
export default withStore;
