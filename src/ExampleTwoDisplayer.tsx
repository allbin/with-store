import React, { useEffect } from 'react';
import withStore, { WithStoreProps, Store } from './store';


interface ExampleTwoDisplayerProps extends WithStoreProps<Pick<Store, "example_two">> {}

let mounts = 0;
let render_count = 0;

const ExampleTwoDisplayer = (props: ExampleTwoDisplayerProps) => {
    useEffect(() => {
        mounts++;
        render_count = 0;
    }, []);

    render_count++;



    return (
        <div style={{padding: "20px"}}>
            <p>EXAMPLE TWO DISPLAYER<br/>This component only subscribes to 'example_two' state. But it is a child of Main.</p>
            <div>times mounted: {mounts}</div>
            <div>render_count for current mount: {render_count}</div>
            <br/>
            <div>example_two.letter: {props.store.example_two.letter}</div>
        </div>
    );
};

export default withStore(ExampleTwoDisplayer, ["example_two"]);
