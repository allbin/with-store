import React, { useEffect } from 'react';
import withStore, { WithStoreProps, Store } from './store';

import ExampleTwoDisplayer from './ExampleTwoDisplayer';

interface MainProps extends WithStoreProps<Pick<Store, "example_one">> {
    required_test_prop: boolean;
}

let mounts = 0;
let render_count = 0;

const Main = (props: MainProps) => {
    const num = props.store.example_one.num;
    useEffect(() => {
        mounts++;
        render_count = 0;
    }, []);

    render_count++;

    return (
        <div>
            <p>MAIN<br/>This component only subscribes to 'example_one' state.<br/>
                Render count should not change when pressing below to update 'example_two' state.</p>
            <div>times mounted: {mounts}</div>
            <div>render_count in current mounting: {render_count}</div>
            <br/>
            <div>num: {num}</div>
            <div>extension test: isPositive: {props.store_ext.numIsPositive() ? "true" : "false" }</div>
            <br/>
            <button onClick={() => { props.store_actions.example_one.setNum(num + 1); }}>Update 'example_one' state</button>
            <button onClick={() => { props.store_actions.example_two.nextLetter(); }}>Update 'example_two' state</button>
            <ExampleTwoDisplayer />
        </div>
    );
};

export default withStore(Main, ["example_one"]);
