import * as React from 'react';
import * as ReactDOM from 'react-dom';

import withStore from './store';
import { reducers_and_actions } from './store';
import general_store_extensions from './store/general_extensions';

import Main from './Main';


declare global {
    interface Window {
        store: any;
    }
}

withStore.addStates(reducers_and_actions);
withStore.addExtensions(general_store_extensions);

window.store = withStore;

ReactDOM.render(
    <Main
        required_test_prop={true}
    />,
    document.getElementById('root') as HTMLElement
);
