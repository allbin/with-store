import * as types from './example_one_reducer';

export interface ExampleOneActions {
    setNum: (value: number) => StoreAction;
}

let example_one_actions: ExampleOneActions = {
    setNum: (value) => {
        return {
            type: types.SET_NUM,
            payload: value
        };
    }

};

export default example_one_actions;