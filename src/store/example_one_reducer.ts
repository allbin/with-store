export const SET_NUM = "SET_NUM";

let default_state: ExampleOneState = {
    num: 0
};

export default function devicesReducer(state = default_state, action: StoreAction) {
    switch (action.type) {
        case (SET_NUM): {
            return Object.assign({}, state, { num: action.payload });
        }
        default: {
            return state;
        }
    }
}
