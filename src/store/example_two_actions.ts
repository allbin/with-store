import * as types from './example_two_reducer';

export interface ExampleTwoActionsBase {
    setLetter: (value: string) => StoreAction;
    nextLetter: (store: ExampleTwoState) => StoreAction;
}

export interface ExampleTwoActions extends ExampleTwoActionsBase {
    nextLetter: () => StoreAction;
}

let example_two_actions: ExampleTwoActionsBase = {
    setLetter: (value) => {
        return {
            type: types.SET_LETTER,
            payload: value
        };
    },
    nextLetter: (state) => {
        let char = state.letter.charCodeAt(0);
        return {
            type: types.SET_LETTER,
            payload: (char > 89) ? 'A' : String.fromCharCode(char + 1)
        };
    }
};

export default example_two_actions;
