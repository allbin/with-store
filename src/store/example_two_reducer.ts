export const SET_LETTER = "SET_LETTER";

let default_state: ExampleTwoState = {
    letter: "A"
};

export default function devicesReducer(state = default_state, action: StoreAction) {
    switch (action.type) {
        case (SET_LETTER): {
            return Object.assign({}, state, { letter: action.payload });
        }
        default: {
            return state;
        }
    }
}
