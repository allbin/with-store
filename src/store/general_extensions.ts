import { IStoreExtensions, IStoreExtension } from '../module';

interface GeneralExtensionsBase extends IStoreExtensions {
    getNum: IStoreExtension;
    numIsPositive: IStoreExtension;
}

export interface GeneralExtensions {
    getNum: () => number;
    numIsPositive: () => boolean;
}

let extensions: GeneralExtensionsBase = {
    getNum: {
        states: ["example_one"],
        mnemonic: "never",
        fn: (states: LooseObject, user_id: null | number = null) => {
            let example_one_state = states.example_one as ExampleOneState;
            return example_one_state.num;
        }
    },
    numIsPositive: {
        states: ["example_one"],
        mnemonic: "always",
        resetCB: () => {
            console.log("numIsPositive extension is subscribing to 'example_one' state and had its mnemonic result reset.");
        },
        fn: (states: LooseObject) => {
            let example_one_state = states.example_one as ExampleOneState;
            return example_one_state.num >= 0;
        }
    }
};

export default extensions;
