import withStoreBase, { IReducersAndActions } from '../module';
import example_one_actions, { ExampleOneActions } from './example_one_actions';
import example_one_reducer from './example_one_reducer';
import example_two_actions, { ExampleTwoActions } from './example_two_actions';
import example_two_reducer from './example_two_reducer';
import { GeneralExtensions } from './general_extensions';


export interface Store {
    readonly example_one: ExampleOneState;
    readonly example_two: ExampleTwoState;
}

export interface StoreActions {
    readonly example_one: ExampleOneActions;
    readonly example_two: ExampleTwoActions;
}

export interface WithStoreProps<P = Store> {
    readonly store: P;
    readonly store_actions: StoreActions;
    readonly store_ext: GeneralExtensions;
}

export default withStoreBase;



let reducers_and_actions: IReducersAndActions = {
    example_one: { actions: { ...example_one_actions }, reducer: example_one_reducer },
    example_two: { actions: { ...example_two_actions }, reducer: example_two_reducer },
};

export { reducers_and_actions };
