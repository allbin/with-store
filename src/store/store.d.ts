interface StoreAction {
    type: string;
    payload?: { [key: string]: any; } | string | number;
}

interface ExampleOneState {
    num: number;
}
interface ExampleTwoState {
    letter: string;
}

interface LooseObject {
    [key: string]: any;
}
